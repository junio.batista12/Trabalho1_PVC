#include "Image.h"


Image::Image() {
	colortopaint[2] = 255;
	colortopaint[1] = 0;
	colortopaint[0] = 0;
	select = false;
}

Image::~Image() {
}

void Image::initializeCam() {
	capture.open(0);
	namedWindow("Camera");
	windowName = "Camera";
	setMouseCallback("Camera", selectPixelVideo, (void*)this);
}

void Image::initializeFile(string filename) {
	ext = filename.substr(filename.find_first_of('.')+1, filename.size());
	this->filename = filename;
	if (ext == "jpg") {
		namedWindow("Imagem");
		windowName = "Imagem";
		frame = imread(filename,CV_LOAD_IMAGE_ANYCOLOR);
		setMouseCallback("Imagem", selectPixelPic, (void*)this);
		cout << frame.channels() << endl;
		if(isGray()) {
			type = "GRAY";
		}
		else {
			type = "BGR";
		}
	}
	else {
		namedWindow("Video");
		windowName = "Video";
		capture = VideoCapture(filename);
		setMouseCallback("Video", selectPixelVideo, (void*)this);
	}
}

void Image::getFrame() {
	if (capture.isOpened()) {
		capture >> frame;
	}
}

void Image::showFrame() {
	if (!frame.empty()) {
		imshow(windowName, frame);
	}
}


bool Image::calculaDistancia(Vec3b a, Vec3b b, int max) {
	int cr, cg, cb;
	cr = a[2] - b[2];
	cg = a[1] - b[1];
	cb = a[0] - b[0];
	if (sqrt(cr*cr + cg * cg + cb * cb) <= max) {
		//cout << sqrt(cr*cr + cg * cg + cb * cb) << endl;
		return true;
	}
	else {
		return false;
	}
}

void Image::selectFrame() {
	Vec3b colortest;
	if (select) {
		for (int x = 0; x < frame.cols; x++) {
			for (int y = 0; y < frame.rows; y++) {
				colortest = frame.at<Vec3b>(Point(x, y));
				if (calculaDistancia(colortest, color, 13)) {
					frame.at<Vec3b>(Point(x, y)) = colortopaint;
				}
			}
		}
	}
}

void Image::selectPixelVideo(int event, int x, int y, int, void * ptr) {
	Image * objeto = (Image*)ptr;
	if (event == EVENT_LBUTTONDOWN) {
		objeto->select = true;
		objeto->color = objeto->frame.at<Vec3b>(Point(x, y));
		cout << "row:" << y << " col:" << x << " R:" << (int)objeto->color[2] << " G:" << (int)objeto->color[1] << " B:" << (int)objeto->color[0] << endl;
	}
}

void Image::selectPixelPic(int event, int x, int y, int, void* ptr) {
	Image * objeto = (Image*)ptr;
	objeto->frame = imread(objeto->filename.c_str(), CV_LOAD_IMAGE_ANYCOLOR);
	if (event == EVENT_LBUTTONDOWN) {
		objeto->select = true;
		objeto->color = objeto->frame.at<Vec3b>(Point(x, y));
		if (objeto->type == "GRAY") {
			cout << "row:" << y << " col:" << x << "Intensity: " << (int)objeto->color[2] << endl;
		}
		else {
			cout << "row:" << y << " col:" << x << " R:" << (int)objeto->color[2] << " G:" << (int)objeto->color[1] << " B:" << (int)objeto->color[0] << endl;
		}
		objeto->selectFrame();
		imshow(objeto->windowName, objeto->frame);
	}
}

bool Image::isGray() {
	Vec3b colortest;
	for (int x = 0; x < frame.cols; x++) {
		for (int y = 0; y < frame.rows; y++) {
			colortest = frame.at<Vec3b>(Point(x, y));
			if (colortest[0] != colortest[1] || colortest[1] != colortest[2]) {
				return false;
			}
		}
	}
	return true;
}
