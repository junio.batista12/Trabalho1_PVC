cmake_minimum_required(VERSION 2.8.12)

project(Trabalho1_PVC)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

add_executable(Trabalho1_PVC "Trabalho_PVC.cpp" "Image.cpp")
target_link_libraries(Trabalho1_PVC ${OpenCV_LIBS})
