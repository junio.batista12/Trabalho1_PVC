#include "Image.h"

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {
	string filename;
	Image image;
	int key=(int)'a';
	switch (argc) {
		case 1:
			image.initializeCam();
			while ((char)key != 'q' && (char)key != 'Q') {
				image.getFrame();
				image.selectFrame();
				image.showFrame();
				key = waitKey(1);
			}
			break;
		case 2:
			filename = argv[1];
			image.initializeFile(filename);
			while ((char)key != 'q' && (char)key != 'Q') {
				image.getFrame();
				image.selectFrame();
				image.showFrame();
				key = waitKey(17);
			}
			break;
		default:
			break;
	}
    return 0;
}
