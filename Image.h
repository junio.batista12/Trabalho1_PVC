#pragma once
#include <string>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <math.h>

using namespace std;
using namespace cv;

class Image {
public:
	Image();
	~Image();
	void initializeCam();
	void initializeFile(string);
	void getFrame();
	void showFrame();
	void selectFrame();
	static void selectPixelVideo(int, int, int, int, void*);
	static void selectPixelPic(int, int, int, int, void*);
private:
	bool isGray();
	string filename, type, windowName, ext;
	Mat frame;
	bool select;
	bool calculaDistancia(Vec3b, Vec3b, int);
	Vec3b color, colortopaint;
	VideoCapture capture;
};
