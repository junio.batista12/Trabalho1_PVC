﻿# Projeto demonstrativo 1 - Visão Computacional
> Aluno: Divino Júnio Batista Lopes

> Matrícula: 15/0033443

## Introdução
> O projeto visa fazer uma breve introdução às funcionalidades da OpenCV desenvolvendo uma simples aplicação utilizando tal biblioteca. A aplicação consiste em dois passos: ao usuário clicar em um pixel da imagem de entrada (que pode ser vinda da própria webcam, uma imagem colorida, uma imagem em grayscale ou um vídeo em formato AVI), deve-se imprimir no terminal a linha, a coluna e os valores dos subpixels do pixel selecionado e, após isso, pintar todos os pixels da imagem que possuir a cor a uma distância euclidiana igual ou menor a 13 da cor do pixel selecionado.

## Descrição dos softwares utilizados
> Linguagem de programação: C++

> Versão do compilador: MSVC 19.14.26433 no Windows e g++ 5.4.0 20160609

> Versão da OpenCV: 3.4.2 no Windows e 3.2.0 no Ubuntu

> Versão do CMake: 3.12.1

> Versão do Visual Studio: Community 15 2017

## Instalando requisitos
### Ubuntu
> Para instalar no Ubuntu, basta utilizar o [script disponibilizado pelo usuário milq em seu GitHub](https://github.com/milq/milq/blob/master/scripts/bash/install-opencv.sh) através dos comandos no terminal:
```sh
cd
cd Downloads
mkdir opencv
cd opencv
wget https://raw.githubusercontent.com/milq/milq/master/scripts/bash/install-opencv.sh
chmod +x install-opencv.sh
./install-opencv.sh
```
> OBS.: No script citado é criada a variável __OPENCV_VERSION__, caso queira baixar outra versão, que não a 3.4.2, basta modificar esse valor.

### Windows
> Para a instalação no Windows será necessário que se baixe:

> [Microsoft Visual Studio 2017](https://visualstudio.microsoft.com/vs/community/) (Não tão necessário, mas fortemente recomendável)

> [OpenCV 3.4.2](https://github.com/opencv/opencv/archive/3.4.2.zip)

> [OpenCV Contrib 3.4.2](https://github.com/opencv/opencv_contrib/archive/3.4.2.zip)(Opcional)

> [CMake 3.12.1-x64](https://cmake.org/files/v3.12/cmake-3.12.1-win64-x64.msi) ou [x86](https://cmake.org/files/v3.12/cmake-3.12.1-win32-x86.msi)
##### 1° Passo
Na raíz do HD, crie uma pasta chamada opencv342 e dentro dela crie outra pasta chamada build.
##### 2° Passo
Dentro da pasta opencv342, descompacte tanto o OpenCV quanto o OpenCV Contrib.
##### 3° Passo
Abra o CMake e especifique o caminho dos sources (C:\opencv342\opencv-3.4.2) e o caminho para build (C:\opencv342\build) e clique em *Configure* (Nesse momento aparecerá uma caixa perguntando para qual gerador será, porém por padrão já fica selecionado o Visual Studio 15 2017, portanto basta clicar em Finish).
##### 4° Passo
Procure por uma variável chamada **OPENCV_EXTRA_MODULES_PATH**, configure seu valor como *C:\opencv342\opencv_contrib-3.4.2*, clique novamente em *Configure* e depois em *Generate*.
##### 5° Passo
Dentro do Visual Studio, abra a Solução *OpenCV.sln*. Com a configuração em Debug, compile o projeto *ALL_BUILD*. Mude para Release e compile o mesmo projeto novamente. Repita o procedimento, porém dessa vez com o projeto *INSTALL*.

## Compilando e executando
### Ubuntu
> Para compilar no Ubuntu, basta rodar os seguintes comandos no termial:
```sh
mkdir Trabalho1_PVC
cd Trabalho1_PVC
git clone https://gitlab.com/junio.batista12/Trabalho1_PVC
mkdir build
cd build
cmake ../Trabalho1_PVC
make
```
> Para rodar, existem 3 possibilidades:
```sh
./Trabalho1_PVC #Para utilizar a camera
./Trabalho1_PVC ***.jpg #Para utilizar uma imagem JPG (Grayscale ou RGB)
./Trabalho1_PVC ***.avi #Para utilizar um video no formato AVI
```